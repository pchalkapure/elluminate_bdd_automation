﻿using System;
using Excel = Microsoft.Office.Interop.Excel;
using Automation_Elluminate;
namespace Automation_Elluminate
{
    public class ExcelInfo
    {

        public static void ReadTestData(String SheetName)
        {
            Excel.Application app = new Excel.Application();

            try
            {
                
                Excel.Workbook wb = app.Workbooks.Open("StartUp.TestDataLocation", 0, true, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                Excel.Worksheet sheet = (Microsoft.Office.Interop.Excel.Worksheet)wb.Sheets[SheetName];

                Excel.Range aRange = null;
                aRange = sheet.UsedRange;
                int rowCount = aRange.Rows.Count;
                int colCount = aRange.Columns.Count;

                Variables.iterationCount = aRange.Rows.Count-1;

                //genarate data keyvalues

                for (int i = 1; i <= aRange.Columns.Count; i++)
                {
                    var cellValue = (string)(sheet.Cells[1, i] as Excel.Range).Value2;

                    Variables.DataSetkey.Add(cellValue.ToUpper());
                }

                for (int i = 2; i <= aRange.Rows.Count; i++)
                {
                    for (int j = 1; j <= aRange.Columns.Count; j++)
                    {
                        var cellValue = (sheet.Cells[i, j] as Excel.Range).Value2;

                        Variables.DataSetvalues.Add(cellValue);

                    }
                }

                int k = 0;
                int l = 1;
                while (k < Variables.DataSetvalues.Count)
                {
                    for (int j = 0; j < aRange.Columns.Count; j++)
                    {

                        String key = Variables.DataSetkey[j].ToString() + l;
                        String value = Variables.DataSetvalues[k].ToString();
                        Variables.DataSetTable.Add(key, value);
                        k++;
                    }

                    l++;

                }
                app.Quit();
                Console.WriteLine("ReadTestData successfully executed");
            }
            catch (Exception ex)
            {
                app.Quit();
                Variables.isTestCaseFailed = true;
                throw new mkExceptionHandler("ReadTestData", ex, ex.Message);
            }
        }


           
    }
    
}
