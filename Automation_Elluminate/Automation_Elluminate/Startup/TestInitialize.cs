﻿using BoDi;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Configuration;
using System.IO;
using TechTalk.SpecFlow;
using Automation_Elluminate;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using RelevantCodes.ExtentReports;
using OpenQA.Selenium.Remote;

namespace Automation_Elluminate.StepDefinitions
{
    [Binding]
    public class Events
    {
        

        public Events(IObjectContainer objectContainer)
        {
            
        }

        [BeforeTestRun]
        public static void TestBefore()
        {
            ReportStart();
        }

        [AfterTestRun]
        public static void TestAfter()
        {
            Report.extent.Close();

        }

        [BeforeScenario]
        public static void StartWebDriver()
        {
            
            Variables.currentScenarioName = ScenarioContext.Current.ScenarioInfo.Title;
            Report.test = Report.extent.StartTest(Variables.currentScenarioName);            
            if (Startupfile.Browser == "CHROME")
            {
                ChromeOptions options = new ChromeOptions();
                options.AddArguments("test-type");
                options.AddArguments("disable-infobars");
                options.AddUserProfilePreference("credentials_enable_service", false);
                options.AddUserProfilePreference("profile.password_manager_enabled", false);
                options.AddUserProfilePreference("Proxy", "null");
                options.AddUserProfilePreference("excludeSwitches", "ignore-certificate-errors");
                options.AddArgument("--log-level=3");                
                ElementInfo.driver = new ChromeDriver(Startupfile.ChromeWebDriverPath, options, TimeSpan.FromSeconds(480));               
                Console.WriteLine("Chrome driver is intialized ");
                ElementInfo.driver.Manage().Window.Maximize();
            }
            else if (Startupfile.Browser == "FIREFOX")
            {
               
                FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\WebDriver");                
                ElementInfo.driver = new FirefoxDriver(service);
               

            }
            else if (Startupfile.Browser == "IE")
            {
                handleProxySettings();

                InternetExplorerOptions capabilities = new InternetExplorerOptions();
                capabilities.AddAdditionalCapability("NATIVE_EVENTS", false);
                

                ElementInfo.driver = new InternetExplorerDriver(@"C:\Selluminate\Selluminate\references", capabilities);
                Console.WriteLine("IE driver is intialized ");
            }
            else
            {
                Console.WriteLine("Webdriver can't intialized without browser : please provide browserName ");
            }
            Console.WriteLine("BeforeTestRun");
          
        }

        /* If proxy is enable it'll disable the proxy else it ignore the step*/
        public static void handleProxySettings()
        {
            string internetSettingsPath = @"Software\Microsoft\Windows\CurrentVersion\Internet Settings";

            Microsoft.Win32.RegistryKey regk = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(internetSettingsPath, true);

            int proxyValue = int.Parse(regk.GetValue("ProxyEnable").ToString());

            if (proxyValue == 1)
            {
                regk.SetValue("ProxyEnable", 0, Microsoft.Win32.RegistryValueKind.DWord);

                Console.WriteLine("Proxy settings disabled.");

                ElementInfo.driver.Navigate().Refresh();
            }

            regk.Close();

        }


        [BeforeFeature]
        public static void StartWebDriverForReal()
        {
            try
            {
                var featureTitle = FeatureContext.Current.FeatureInfo.Title;
                var featureTags = FeatureContext.Current.FeatureInfo.Tags;
                var featureDescription = FeatureContext.Current.FeatureInfo.Description;

                Console.WriteLine("BeforeFeature");
            }
            catch (Exception )
            {
                Console.WriteLine("BeforeFeature");
            }
        }



        //[BeforeScenario(Order =2)]
        //public void GoToHomePage1()

        //[BeforeScenario(Order = 1)]
        //public void GoToHomePage12()

        [BeforeStep]
        public static void BeforeStep()
        {
            Console.WriteLine("BeforeStep");
        }

        [AfterStep]
        public static void AfterStep()
        {
            if (Variables.isTestCaseFailed)
            {
                TestReport.Takescreenshot("Failed", ScenarioContext.Current.StepContext.StepInfo.Text);
            }
        }

        [AfterScenario]
        public static void AfterScenario()
        {

            ElementInfo.driver.Quit();
            ReportEnd();

            if (Variables.isTestCaseFailed)
            {
                ReInitializeValues();
                Assert.Fail(Report.ErrorMessage);
                Console.WriteLine("Failed due to" + Report.ErrorMessage);
            }


            ReInitializeValues();
            


           // ElementInfo.driver.Close();
            //ScenarioContext.Current.ScenarioInfo.Title + "_" +
            //ScenarioContext.Current.TestError != null
            //ScenarioContext.Current.TestError.TargetSite.Name + "_" +
            // ScenarioContext.Current.TestError.Message + "_" +
        }
            

        

        [AfterFeature]
        public static void killBrowser()
        {
            Console.WriteLine("AfterFeature");

        }

        [AfterTestRun]
        public static void TerminateWebDriver()
        {
            Console.WriteLine("AfterTestRun");
        }

        

        /*public static ChromeOptions MakeHeadless(ChromeOptions chromeOptions)
        {
            IWebDriver driverToGetWindowSizeFrom = new ChromeDriver();
            driverToGetWindowSizeFrom.Manage().Window.Maximize();
            int height = driverToGetWindowSizeFrom.Manage().Window.Size.Height;
            int width = driverToGetWindowSizeFrom.Manage().Window.Size.Width;
            driverToGetWindowSizeFrom.Quit();

            //Need to specify BinaryLocation if using Headless Chrome.
            chromeOptions.BinaryLocation = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe";
            chromeOptions.AddArgument("--headless"); //Only Availble in dev release of chrome.
            chromeOptions.AddArgument("--disable-gpu"); //Also needed for headless version.
            //Driver.Manage().Window.Maximize(); Does not work in headless mode.
            //This Chrome should start in full screen.
            chromeOptions.AddArgument("window-size=" + width + "," + height);

            return chromeOptions;
        }*/


        public static void ReportStart()
        {

            if (!System.IO.Directory.Exists(Report.reportPath))
            {
                System.IO.Directory.CreateDirectory(Report.reportPath);
            }

            String TestReport = Report.reportPath + "\\elluminateTestReport" + DateTime.Now.ToString("MM_dd_yyyy_HH_mm_ss") + ".html";

            Report.extent = new ExtentReports(TestReport);

        }

        public static void ReportEnd()
        {


            if (!Variables.isTestCaseFailed)
            {
                Report.test.Log(LogStatus.Pass, "Assert Pass as condition is True");
            }
            else
            {
                // Report.test.AddScreenCapture(Report.reportPath + "\\"+Variables.currentTestcaseName+ Variables.iteration+ ".PNG");
                Report.test.Log(LogStatus.Fail, Report.test.AddScreenCapture(Report.reportPath + "\\" + Variables.currentScenarioName  + ".PNG"));
            }


            Report.extent.EndTest(Report.test);
            Report.extent.Flush();

        }

        public static void ReInitializeValues()
        {
            Variables.DataSetkey.Clear();
            Variables.DataSetvalues.Clear();
            Variables.DataSetTable.Clear();
            Variables.isTestCaseFailed = false;
            Report.ErrorMessage = null;
        }

    }
}
