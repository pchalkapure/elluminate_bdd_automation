﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate
{
    class Startupfile
    {

        public static string Environment = "TesturlC"; //set 'DEV' 'TesturlC' and 'TesturlD' 
        public static string Browser = "CHROME";  //' CHROME ', 'IE' and FIREFOX
        public static string DevUrl = "https://dev-next-release.eclinicalsol.com";
        public static string TesturlC = "https://testc.eclinicalsol.com";
        public static string TesturlD = "https://testd.eclinicalsol.com";
        public static String ChromeWebDriverPath = @"C:\WebDriver\";

        public static String TestDataLocation = @"C:\Elluminate_Specflow\Automation_Elluminate\Automation_Elluminate\TestData\TestData.xlsx";
        public static String PasswordProtectTestData = @"C:\Elluminate_Specflow\Automation_Elluminate\Automation_Elluminate\TestData\PasswordProtect.zip";
        public static String ManualFileImportExcel = @"C:\Specflow_Automation\Automation_Elluminate\Automation_Elluminate\TestData\ImportExcelFile.xlsx";
        public static String CMFile = @"C:\Elluminate_Specflow\Automation_Elluminate\Automation_Elluminate\TestData\CM.xlsx";
        public static String ManualFileImportSASXPORT = @"C:\Specflow_Automation\Automation_Elluminate\Automation_Elluminate\TestData\SASXPORT.zip";
        public static String DefineXMLData = @"C:\Specflow_Automation\Automation_Elluminate\Automation_Elluminate\TestData\Define-xml-data.xlsx";
    }
}
