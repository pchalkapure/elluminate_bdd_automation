﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate
{
   
   
    class MetaData
    {
        public static string StudiesHeader = "XPATH^//h3[text()='Studies']";
        public static string Browse = "XPATH^//span[text()='Browse']";
        public static string Search = "XPATH^//span[text()='Search']";
        public static string Compare = "XPATH^//span[text()='Compare']";
        public static string Browse_DataStore = "XPATH^//*[@id='metadata-browse-top']//table/tbody/tr[1]/td[1]/span";
        public static string Browse_Domains = "XPATH^//*[@id='metadata-browse-top']//table//table/tbody/tr[1]/td[1]/span";
        public static string SelectStudy = "XPATH^//div[@id='metadata-simple-left-pane']//div[@title='StudyName']";

        public static string Browser_Expand_DataStore = "XPATH^//span[@class='fa fa-caret-right']";
        public static string Browser_DSName = "XPATH^//span[@ng-bind='dataStore.Name']";
        public static string Browser_DSDomain = "XPATH^//span[@ng-bind='dataStore.Domains']";
        public static string Browser_DSVariable = "XPATH^//span[@ng-bind='dataStore.Variables']";
        public static string Browser_Excel = "XPATH^//span[@title='Download as Excel']";
        public static string Browser_XML = "XPATH^//button[@title='Export to Xml']";
        public static string Browser_Expand_Domain = "XPATH^//tr[@ng-if='dataStore.expanded']//tbody/tr[1]/td[1]/span[@class='fa fa-caret-right']";


        public static string Search_ForDomains = "XPATH^//input[@value='ForDomains']";
        public static string Search_ForVariables = "XPATH^//input[@value='ForVariables']";
        public static string Search_WithinLabels = "ID^WithinLabels";
        public static string Search_WithinNames = "ID^WithinNames";
        public static string Search_ConditionStartsWith = "XPATH^//input[@value='ConditionStartsWith']";
        public static string Search_ConditionEndsWith = "XPATH^//input[@value='ConditionEndsWith']";
        public static string Search_ConditionContains = "XPATH^//input[@value='ConditionContains']";
        public static string Search_ConditionEqual = "XPATH^ //input[@value='ConditionEqual']";
        public static string Search_SearchText = "ID^SearchText";
        public static string Search_Submit = "ID^metadata-search-submit";
        //public static string Search_Table = "XPATH^//div[@class='t-grid-content']/table//tbody/tr/td[3]";
        public static string Search_Table = "//*[@id='metadata-searchresult-grid']/div[5]/table/tbody/tr";
        public static string Search_Study = "XPATH^//*[@id='metadata-simple-multi-select-studies']//h4/span[contains(text(),'Study')]";

        public static string Compare_Staging = "ID^show-staging";
        public static string Compare_DataMart = "ID^show-marts";
        public static string Compare_Study = "XPATH^//span[text()='Study']/preceding-sibling::span/input[2]";
        public static string Compare_Domain = "XPATH^//span[text()='Domain']/preceding-sibling::span/input[2]";
        public static string Compare_Table = "//*[@id='metadata-compareresult-grid']/div[3]/table/tbody/tr";

    }

}
