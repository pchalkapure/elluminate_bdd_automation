﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate
{
    
    class AddStudy
    {
        public static string StudiesPage = "XPATH^//h2[text()='Studies']";
        public static string VerifyStudy = "XPATH^//div/table/tbody//td[contains(text(),'Automation_Compound')]";
        public static string DeleteStudy = "XPATH^//div[@id='#snapgridtileview']/div[1]//div/span/span[2]/a/i";
        public static string DelStudyCheckbox = "XPATH^//input[@id='confirmCheckBox']";
        public static string YesButton = "XPATH^//button[text()='Yes']";
        public static string AddStudylink = "ID^newItemButton";
        public static string StudyId = "Id^name";
        public static string CompoundId = "Id^compound";
        public static string SaveStudy = "XPATH^//button[text()='Save']";
        public static string ClickStudy = "XPATH^//div[@id='#snapgridtileview']/div[1]//h1/a[contains(text(),'Study')]";
        public static string searchfilter = "ID^search";
        public static string tileview = "XPATH^//div[@title='Tile View']";
        public static string Searchstudy = "XPATH^//button[@type='submit']";
        //public static string studylink = "XPATH^//a[@title='ENR']";
    }
}
