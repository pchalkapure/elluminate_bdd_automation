﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate
{
   
    class Compounds
    {
        public static string Compoundlink= "XPATH^//span[text()='Compounds']";
        public static string AddCompound = "XPATH^//a[contains(text(),'Add Compound')]";
        public static string CompoundName = "ID^Name";
        public static string CompoundDiscription = "ID^Comments";
        public static string Save = "XPATH^//input[@value='Save']";
        public static string Cancel = "XPATH^//input[@value='Cancel']";
        public static string CompoundTable = "//*[@id='compounds-grid']/div[3]/table/tbody/tr";
        public static string DeleteButton = "XPATH^//input[@value='Delete']";
    }

    class StagingAreas
    {
        public static string StagingAreaslink = "XPATH^//span[text()='Staging Areas']";
        public static string AddStagingArea = "XPATH^//a[contains(text(),'Add Staging Area')]";
        public static string Name = "XPATH^//input[@name='Name']";
        public static string Discription = "ID^Description";
        public static string SchemaSuffix = "XPATH^//input[@name='SchemaSuffix']";
        public static string Save = "XPATH^//input[@value='Save']";
        public static string Cancel = "XPATH^//input[@value='Cancel']";
        public static string StagingAreaTable = "//*[@id='StagingArea-grid']/div[3]/table/tbody/tr";
        public static string DeleteButton = "XPATH^//input[@value='Delete']";
    }

    class DataMarts
    {
        public static string DataMartslink = "XPATH^//span[text()='Data Marts']";
        public static string AddDataMarts = "XPATH^//a[contains(text(),'Add Data Mart')]";
        public static string Name = "XPATH^//input[@name='Name']";
        public static string Discription = "ID^Description";
        public static string SchemaSuffix = "XPATH^//input[@name='SchemaSuffix']";
        public static string Save = "XPATH^//input[@value='Save']";
        public static string Cancel = "XPATH^//input[@value='Cancel']";
        public static string DataMartsTable = "//*[@id='Datamart-grid']/div[3]/table/tbody/tr";
        public static string DeleteButton = "XPATH^//input[@value='Delete']";
    }

}
