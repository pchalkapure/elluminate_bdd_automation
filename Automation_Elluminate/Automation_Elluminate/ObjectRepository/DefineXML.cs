﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate
{
    class DefineXML
    {
        public static string DefineXMLHeader = "XPATH^//span[text()='Define XML']";
        public static string Select_Btn = "XPATH^//div[@class='t-button t-upload-button btn btn-primary']";
        public static string Generate_Btn = "XPATH^//button[text()='Generate']";
        public static string Validate_Checkbox = "ID^define-validate";
        public static string Verify_Status = "XPATH^//div[@class='t-grid-content']/table/tbody/tr[1]/td[4][text()='Complete']";
        public static string TableGrid = "//div[@class='t-grid-content']/table/tbody/tr";
        public static string Validation_Result = "//div[@class='t-grid-content']/table/tbody/tr/td[3]/a[contains(text(),'validation')]";
        public static string All_Result = "//div[@class='t-grid-content']/table/tbody/tr/td[3]/a[contains(text(),'')]";
        public static string Click_ValidationLink = "XPATH^//div[@class='t-grid-content']/table/tbody/tr[1]/td[3]/a";
        

    }
}
