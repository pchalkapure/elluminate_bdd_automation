﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate
{
    class DataQuality
    {
        public static string DataQualityHeader = "XPATH^//a[@data-name='data-quality']";
        public static string Verify_DQPage = "XPATH^//h2[text()='Data Quality Jobs']";
        public static string DQListView = "XPATH^//div[@aria-label='list view']";
        public static string DQTitleView = "XPATH^//div[@aria-label='tile view']";
        public static string ValidationJobs = "XPATH^//span[text()='Validation Jobs']";

        public static string Validations = "XPATH^//span[text()='Validations']";
        public static string Validate = "ID^study-validate-button";
        public static string ValidateConfirm = "ID^domains-submit";
        public static string ValidateCancel = "ID^domains-cancel";
        public static string ValidateDelete = "XPATH^//table/tbody/tr[1]/td/div/span[@title='Delete']";
        public static string DelConCheckbox = "ID^confirmCheckBox";
        public static string ValidateDeleteConfirm = "ID^validationjob-delete-submit";
        public static string ValidateStatus = "XPATH^//table/tbody/tr[1]/td[text()='Completed']";
        public static string ValidationTable = "//div[@class='t-grid-content']/table/tbody/tr";

        public static string AddJob = "ID^newItemButton";
        public static string DQName = "ID^name";
        public static string DQDataStore = "//option[text()='DataStore']";
        public static string DQDomain = "//option[text()='Domain']";
        public static string DQMoveRight = "ID^domain-moveright";
        public static string DQMoveAllRight = "ID^domain-moveallright";
        public static string DQSave = "ID^snapshot-add-save";
        public static string DQCancel = "ID^snapshot-add-cancel";
        public static string DQRun = "XPATH^//span[@ng-if='$tileController.isRunnable()']";
        public static string DQSchedule = "XPATH^//span[@ng-if='$tileController.isSchedulable()']";
        public static string DQEdit = "XPATH^//span[@ng-if='$tileController.isEditable()']";
        public static string DQDelete = "XPATH^//span[@ng-if='$tileController.isDeletable()']";
        public static string DQDelConfirm = "XPATH^//button[text()='Yes']";
        public static string DQDelCancel = "XPATH^//button[text()='No']";
        public static string DQStatus = "XPATH^//span[contains(text(),'Completed')]";
        public static string DQDelListView = "XPATH^//tbody/tr[1]/td/span/span[@ng-if='$tileViewController.isDeletable(item)']";

        public static string ValidationResults = "XPATH^//span[text()='Validation Results']";
        public static string ValidationDataStore = "XPATH^//table/tbody/tr/td[@id='selected-validation-datamart']";
        public static string DomainChecked = "XPATH^//div[@id='dataquality-domains-tree']/ul/li/div/span/input[@checked='checked']";
        public static string SummaryTab = "XPATH^//ul[@class='nav nav-tabs']/li/a/span[text()='Summary']";
        public static string ResultsTab = "XPATH^//ul[@class='nav nav-tabs']/li/a/span[text()='Results']";
        public static string RulesTab = "XPATH^//ul[@class='nav nav-tabs']/li/a/span[text()='Rules']";
        public static string ValidRulesGrid = "//div[@id='validation-rules-grid']/div/table/tbody/tr";
        public static string InfoChkbox = "XPATH^//td[text()='Information']";
        public static string WarningChkbox = "XPATH^//td[text()='Warning']";
        public static string ErrChkbox = "XPATH^//td[text()='Error']";

        public static string ScheduleRepeat = "//input[@type='radio' and @value='Schedule']";

        public static string ValidationRows = "//div[@class='t-grid-content']/table/tbody/tr";
        public static string DownloadXMLFile = "//div[@class='t-grid-content']/table/tbody/tr[1]/td/div/span[@title='Download XML']";
    }
}
