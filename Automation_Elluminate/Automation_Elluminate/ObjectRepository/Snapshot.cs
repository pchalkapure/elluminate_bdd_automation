﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate.ObjectRepository
{
    class Snapshot
    {
        public static string SnapshotHeader = "XPATH^//a[@data-name='snapshot']";
        public static string Verify_SnapshotPage = "XPATH^//h2[text()='Snapshots']";
        public static string Add_Snapshot = "ID^snapshot-new";
        public static string Edit_Snapshot = "XPATH^//span[@class='pull-right margin-right-md']/i[@id='snapshot-edit']";
        public static string Del_Snapshot = "XPATH^//span[@class='pull-right margin-right-md']/i[@id='snapshot-delete']";
        public static string DelConfirm_Snapshot = "XPATH^//button[@class='btn btn-primary' and text()='Yes']";
        public static string DelCancel_Snapshot = "XPATH^//button[@class='btn btn-secondary' and text()='No']";
        // Snapshot Details
        public static string Name = "ID^basic-input-title";
        public static string Description = "ID^basic-input-description";
        public static string Save_Snapshot = "ID^snapshot-add-save";
        public static string Cancel_Snapshot = "ID^snapshot-add-cancel";
        public static string VrfySuccess_Snapshot = "XPATH^//span[@class='ng-binding text-success']";
        public static string EditSave_Snapshot = "ID^snapshot-edit-save";
       


    }
}
