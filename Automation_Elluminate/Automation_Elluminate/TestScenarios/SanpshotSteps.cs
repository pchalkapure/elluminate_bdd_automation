﻿using Automation_Elluminate.ObjectRepository;
using System;
using TechTalk.SpecFlow;

namespace Automation_Elluminate.TestScenarios
{
    [Binding]
    public class SnapshotSteps
    {
        [Given(@"Go to Snapshot tab")]
        public void GivenGoToSnapshotTab()
        {
            Actiontype.clickAndWait(Snapshot.SnapshotHeader, "3000");
            Actiontype.VerifyElementPresent(Snapshot.Verify_SnapshotPage);
            Console.WriteLine("Navigation to Snapshot page successful");
        }

        [Then(@"create new Snapshot ""(.*)""")]
        public void ThenCreateNewSnapshot(string SnapshotName)
        {
            Actiontype.clickAndWait(Snapshot.Add_Snapshot, "2000");
            Actiontype.EnterValue(Snapshot.Name, SnapshotName);
            Actiontype.EnterValue(Snapshot.Description, "Snapshot description text");
            Actiontype.clickAndWait(Snapshot.Save_Snapshot, "3000");
            Actiontype.WaitUntilElementPresent(Snapshot.VrfySuccess_Snapshot,8000);
            Console.WriteLine("Snapshot creation successful");
        }

        [Then(@"Edit the Snapshot")]
        public void ThenEditTheSnapshot()
        {
            Actiontype.clickAndWait(Snapshot.Edit_Snapshot, "2000");
            Actiontype.EnterValue(Snapshot.Name, "Snapshot_Nov_2017");
            Actiontype.EnterValue(Snapshot.Description, "Snapshot description edited text");
            Actiontype.clickAndWait(Snapshot.EditSave_Snapshot, "3000");
        }

        [Then(@"verify snapshot title ""(.*)""")]
        public void ThenVerifySnapshotTitle(string SnapshotTitle)
        {
            Actiontype.VerifyElementPresent("XPATH^//div[@class='col-lg-3 col-md-3 col-sm-4 col-xs-4 ng-scope']/div/div[1]/h1/span[@title='" + SnapshotTitle + "']");
        }

        [Then(@"Delete the Snapshot")]
        public void ThenDeleteTheSnapshot()
        {
            Actiontype.clickAndWait(Snapshot.Del_Snapshot, "2000");
            Actiontype.clickAndWait(Snapshot.DelConfirm_Snapshot, "4000");
            Actiontype.WaitUntilElementNotPresent("//div[@class='col-lg-3 col-md-3 col-sm-4 col-xs-4 ng-scope']/div/div[1]/h1/span", 5000);
            Console.WriteLine("Snapshot deleted successfully");
        }

    }
}
