﻿using System;
using TechTalk.SpecFlow;

namespace Automation_Elluminate.TestScenarios
{
    [Binding]
    public class DataQualitySteps
    {
        [Given(@"select Data Quality Tab")]
        public void GivenSelectDataQualityTab()
        {
            Actiontype.clickAndWait(DataQuality.DataQualityHeader, "2000");
        }
        
        [Then(@"I should see Data Quality home page")]
        public void ThenIShouldSeeDataQualityHomePage()
        {
            Actiontype.VerifyElementPresent(DataQuality.Verify_DQPage);
            Console.WriteLine("Navigation to Data Quality home page successful");
        }

        [Then(@"Clear the DataQuality data")]
        public void ThenClearTheDataQualityData()
        {
            Functions.DelDataQualityJob(); 
        }

        [Then(@"Select the Tile view")]
        public void ThenSelectTheTileView()
        {
            Actiontype.clickAndWait(DataQuality.DQTitleView, "2000");
        }

        [Then(@"Create ""(.*)"" DataQuality Validation for ""(.*)"" DataStore and ""(.*)"" domain")]
        public void ThenCreateDataQualityValidationForDataStoreAndDomain(string Name, string DSName, string DomainName)
        {
            Actiontype.clickAndWait(DataQuality.AddJob, "2000");
            Actiontype.EnterValue(DataQuality.DQName, Name);
            String DataStore = DataQuality.DQDataStore.Replace("DataStore", DSName);
            Actiontype.clickAndWait(DataStore, "1000");
            String Domain = DataQuality.DQDomain.Replace("Domain", DomainName);
            Actiontype.clickAndWait(Domain, "1000");
            Actiontype.click(DataQuality.DQMoveRight);
            Actiontype.clickAndWait(DataQuality.DQSave, "2000");
            Actiontype.VerifyElementPresent("XPATH^//h1/span[text()='" + Name + "']");
            Console.Write("Data Quality Validation creation successful");
        }

        [Then(@"Run the Data Quality job")]
        public void ThenRunTheDataQualityJob()
        {
            Actiontype.clickAndWait(DataQuality.DQRun, "2000");
            Actiontype.WaitUntilElementPresent(DataQuality.DQStatus,9000);
            Console.Write("Data Quality Validation creation successful");
        }

        [Then(@"Edit the Data Quality job as ""(.*)"" DataQuality Validation for ""(.*)"" DataStore and ""(.*)"" domain")]
        public void ThenEditTheDataQualityJobAsDataQualityValidationForDataStoreAndDomain(string Name, string DSName, string DomainName)
        {
            Actiontype.clickAndWait(DataQuality.DQEdit, "2000");
            Actiontype.EnterValue(DataQuality.DQName, Name);
            String DataStore = DataQuality.DQDataStore.Replace("DataStore", DSName);
            Actiontype.clickAndWait(DataStore, "1000");
            String Domain = DataQuality.DQDomain.Replace("Domain", DomainName);
            Actiontype.clickAndWait(Domain, "1000");
            Actiontype.click(DataQuality.DQMoveRight);
            Actiontype.clickAndWait(DataQuality.DQSave, "2000");
            Actiontype.VerifyElementPresent("XPATH^//h1/span[text()='" + Name + "']");
            Console.Write("Data Quality Validation creation successful");
        }
        
        [Then(@"Delete the ""(.*)"" DataQuaity job")]
        public void ThenDeleteTheDataQuaityJob(string Name)
        {
            Actiontype.clickAndWait(DataQuality.DQDelete, "2000");
            Actiontype.clickAndWait(DataQuality.DQDelConfirm, "2000");
            Actiontype.VerifyElementNotPresent("XPATH^//h1/span[text()='" + Name + "']");
            Console.Write("Data Quality Validation deleted successfully");
        }

        [Then(@"Schedule the Data Quality task for ""(.*)""")]
        public void ThenScheduleTheDataQualityTaskFor(string Schedule)
        {
            Actiontype.clickAndWait(DataQuality.DQSchedule, "2000");
            String SelectSched = DataQuality.ScheduleRepeat.Replace("Schedule", Schedule);
            Actiontype.click(SelectSched);
            Actiontype.clickAndWait(ImportFile.ScheduleNext, "1000");
            String SecheduleTime = Actiontype.GetAttributeValue(ImportFile.ScheduleTime, "value");
            DateTime NewSecheduleTime = DateTime.ParseExact(SecheduleTime, "yyyy-MMM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            NewSecheduleTime = NewSecheduleTime.AddHours(-1);
            NewSecheduleTime = NewSecheduleTime.AddMinutes(2);
            String newdate = NewSecheduleTime.ToString("yyyy-MMM-dd HH:mm:ss");
            Actiontype.ClearText(ImportFile.ScheduleTime);
            Actiontype.EnterValue(ImportFile.ScheduleTime, newdate);
            Actiontype.clickAndWait(ImportFile.ScheduleNext, "1000");
            Actiontype.clickAndWait(ImportFile.ScheduleNext, "1000");
            Actiontype.clickAndWait(ImportFile.ScheduleClose, "2000");
            Functions.VeirfyScheduleTask();
        }

        [Then(@"Go to Validations tab")]
        public void ThenGoToValidationsTab()
        {
            Actiontype.clickAndWait(DataQuality.Validations, "2000");           
        }

        [Then(@"Select ""(.*)"" DataStore and ""(.*)"" domain")]
        public void ThenSelectDataStoreAndDomain(string DSName, string DomainName)
        {
            Actiontype.clickAndWait(DataQuality.Validate, "2000");
            String DataStore = DataQuality.DQDataStore.Replace("DataStore", DSName);
            Actiontype.clickAndWait(DataStore, "1000");
            String Domain = DataQuality.DQDomain.Replace("Domain", DomainName);
            Actiontype.clickAndWait(Domain, "1000");
            Actiontype.click(DataQuality.DQMoveRight);          
        }

        [Then(@"Validate Domains")]
        public void ThenValidateDomains()
        {
            Actiontype.clickAndWait(DataQuality.ValidateConfirm, "2000");
            Actiontype.WaitUntilElementPresent(DataQuality.ValidateStatus, 9000);
            Console.Write("Domain Validation successful");
        }

        [Then(@"Cancel Validate Domains")]
        public void ThenCancelValidateDomains()
        {
            Actiontype.clickAndWait(DataQuality.ValidateCancel, "2000");
            Console.Write("Domain Validation cancelled");
        }

        [Then(@"Delete the validation job")]
        public void ThenDeleteTheValidationJob()
        {
            Functions.GetRows(DataQuality.ValidationTable);
            Actiontype.clickAndWait(DataQuality.ValidateDelete, "2000");
            Actiontype.click(DataQuality.DelConCheckbox);
            Actiontype.clickAndWait(DataQuality.ValidateDeleteConfirm, "2000");
            Functions.GetRows(DataQuality.ValidationTable);
            Console.WriteLine("Validation Job deleted successfully");
        }

        [Then(@"Go to Validation Results tab")]
        public void ThenGoToValidationResultsTab()
        {
            Actiontype.clickAndWait(DataQuality.ValidationResults, "2000");
        }

        [Then(@"Verify data store exists and Domain checked")]
        public void ThenVerifyDataStoreExistsAndDomainChecked()
        {
            Actiontype.VerifyElementPresent(DataQuality.ValidationDataStore);
            Actiontype.VerifyElementPresent(DataQuality.DomainChecked);
        }

        [Then(@"Verify result records filtered using Information, Warning and Error")]
        public void ThenVerifyResultRecordsFilteredUsingInformationWarningAndError()
        {
            Actiontype.clickAndWait(DataQuality.RulesTab, "3000");
            Actiontype.clickAndWait(DataQuality.InfoChkbox, "2000");
            Actiontype.clickAndWait(DataQuality.WarningChkbox,"2000");
            Functions.ValidationFilter(DataQuality.ValidRulesGrid);
            Console.WriteLine("Verified all contents successfully");
        }

      

    }
}
