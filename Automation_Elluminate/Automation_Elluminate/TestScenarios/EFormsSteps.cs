﻿using Automation_Elluminate.ObjectRepository;
using System;
using TechTalk.SpecFlow;

namespace Automation_Elluminate.TestScenarios
{
    [Binding]
    public class EFormsSteps
    {
        [Given(@"I select eForms Designer from the Administration DropDown")]
        public void GivenISelectEFormsDesignerFromTheAdministrationDropDown()
        {
            Actiontype.clickAndWait(ElluminateHome.Administration, "2000");
            Actiontype.clickAndWait(ElluminateHome.Administation_eFormsDesigner, "2000");
            Console.WriteLine("Navigation to eFroms successful");
        }

        [Then(@"Verify Study dropdown, PublishALL and AddForm fields exists")]
        public void ThenVerifyStudyDropdownPublishALLAndAddFormFieldsExists()
        {
            Actiontype.VerifyElementPresent(EForms.StudyDrpDwn_Global);
            Actiontype.VerifyElementPresent(EForms.EFormDesgn_Publish);
            Actiontype.VerifyElementPresent(EForms.AddForm);
        }

        [Then(@"Create New Form for ""(.*)"" study and ""(.*)"" Data Store with below details")]
        public void ThenCreateNewFormForStudyAndDataStoreWithBelowDetails(string StudyName, string DataStoreName, Table table)
        {
            Actiontype.clickAndWait(EForms.StudyDrpDwn_Global, "2000");
            String Study = EForms.SelectStudy.Replace("study", StudyName);
            Actiontype.clickAndWait(Study, "3000");
            Actiontype.click(EForms.AddForm);
            Actiontype.EnterValue(EForms.FormName, table.Rows[0][0].ToString());
            Actiontype.EnterValue(EForms.FormLabel, table.Rows[0][1].ToString());
            Actiontype.clickAndWait(EForms.FormDataStore, "2000");
            String DataStore = EForms.SelectDataStore.Replace("DataStore", DataStoreName);
            Actiontype.clickAndWait(DataStore,"1000");
            Actiontype.EnterValue(EForms.FormCategory, table.Rows[0][2].ToString());
            Actiontype.clickAndWait(EForms.SaveForm, "2000");
        }

        [Then(@"Delete the form")]
        public void ThenDeleteTheForm()
        {
            Actiontype.clickAndWait(EForms.DeleteForm, "2000");
            Actiontype.clickAndWait(EForms.DeleteFormConfirm, "2000");
        }


        [Then(@"Add New ""(.*)"" Field into the form with below details")]
        public void ThenAddNewFieldIntoTheFormWithBelowDetails(string Control, Table table)
        {
            Actiontype.clickAndWait(EForms.AddField, "2000");
            Actiontype.EnterValue(EForms.FieldName, table.Rows[0][0].ToString());
            Actiontype.EnterValue(EForms.FieldLabel, table.Rows[0][1].ToString());
            Actiontype.clickAndWait(EForms.FieldCntrlDrpdwn, "2000");
            String Type = EForms.FieldCntrlType.Replace("ControlType", Control);
            Actiontype.clickAndWait(Type, "2000");
            Actiontype.EnterValue(EForms.FieldDisplayOrd, table.Rows[0][2].ToString());
            Actiontype.clickAndWait(EForms.SaveField, "2000");
            Console.WriteLine("Field creation successful");
        }

        [Then(@"Navigate to eForms tab")]
        public void ThenNavigateToEFormsTab()
        {
            Actiontype.clickAndWait(ElluminateHome.eForms, "2000");
            Actiontype.VerifyElementPresent(EForms.VerifyEformPage);
            Console.WriteLine("Navigation to eForms page successful");
        }
        [Then(@"Select the ""(.*)"" study from the drop-down")]
        public void ThenSelectTheStudyFromTheDrop_Down(string StudyName)
        {
            Actiontype.clickAndWait(EForms.StudyDrpDwn_Global, "2000");
            String Study = EForms.SelectStudy.Replace("study", StudyName);
            Actiontype.clickAndWait(Study, "3000");
        }

        [Then(@"Click on ""(.*)"" Category and ""(.*)"" Form")]
        public void ThenClickOnCategoryAndForm(string CategoryName, string FormName)
        {
            //
            String Category = EForms.SelectCategory.Replace("Category", CategoryName);
            Actiontype.clickAndWait(Category, "1000");

            String Form = EForms.SelectForm.Replace("Form", FormName);
            Actiontype.clickAndWait(Form, "1000");
        }

        [Then(@"Create New Record with ""(.*)"" data")]
        public void ThenCreateNewRecordWithData(string Data)
        {
            Actiontype.clickAndWait(EForms.AddRecord, "2000");
            Actiontype.EnterValue(EForms.InputTextData, Data);
            Actiontype.clickAndWait(EForms.SaveForm, "2000");
            Actiontype.VerifyElementPresent(EForms.RecordTable);
            Console.WriteLine("New Record Data added successfully");
        }

        [Then(@"Edit the Record data as ""(.*)""")]
        public void ThenEditTheRecordDataAs(string Data)
        {
            Actiontype.click(EForms.RecordTable);
            Actiontype.clickAndWait(EForms.EditRecord, "2000");
            Actiontype.EnterValue(EForms.InputTextData, Data);
            Actiontype.clickAndWait(EForms.SaveForm, "2000");
            Console.WriteLine("Record Data edited successfully as: "+Data);
        }

        [Then(@"Delete the Record data")]
        public void ThenDeleteTheRecordData()
        {
            Actiontype.click(EForms.RecordTable);
            Actiontype.clickAndWait(EForms.DeleteRecord, "2000");
            Actiontype.clickAndWait(EForms.DelRecConfirm, "2000");
            Actiontype.VerifyElementNotPresent(EForms.RecordTable);
            Console.WriteLine("Record Deleted successfully");
        }

    }

}
