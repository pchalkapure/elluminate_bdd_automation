﻿using System;
using TechTalk.SpecFlow;

namespace Automation_Elluminate.TestScenarios
{
    [Binding]
    public class ConfigurationSteps
    {

        [When(@"I create a new compound called ""(.*)""")]
        public void WhenICreateANewCompoundCalled(string CompoundName)
        {
            Actiontype.clickAndWait(ElluminateHome.Administration, "2000");
            Actiontype.clickAndWait(ElluminateHome.Administation_Configuration, "2000");
            Actiontype.clickAndWait(Compounds.Compoundlink, "2000");
            Actiontype.clickAndWait(Compounds.AddCompound, "2000");
            //Actiontype.click(Compounds.AddCompound);
            Actiontype.EnterValue(Compounds.CompoundName, CompoundName);
            Actiontype.EnterValue(Compounds.CompoundDiscription, CompoundName);
            Actiontype.clickAndWait(Compounds.Save, "5000");
            //TestReport.Takescreenshot("Passed", "I create a new compound called " + CompoundName);
        }


        [When(@"I edit a compound called ""(.*)"" to ""(.*)""")]
        public void ThenIEditACompoundCalledTo(string CompoundName, string NewCompoundName)
        {
            Functions.EditROw(Compounds.CompoundTable, CompoundName, NewCompoundName);

        }

        [When(@"I choose ""(.*)"" as the compound\.")]
        public void WhenIChooseAsTheCompound_(string CompoundName)
        {
            Actiontype.SelectDropdownValue(AddStudy.CompoundId, "TEXT", CompoundName);
        }

        [When(@"I delete a compound called ""(.*)""")]
        public void WhenIDeleteACompoundCalled(string CompoundName)
        {
            Functions.DeleteRow(Compounds.CompoundTable, CompoundName);
        }

        [Then(@"I should see compound""(.*)""")]
        public void ThenIShouldSeecompound(string CompoundName)
        {
            Functions.ValidateRowExist(Compounds.CompoundTable, CompoundName);
        }
        

        [Then(@"I should not see Compound ""(.*)""")]
        public void ThenIShouldNotSeeCompound(string CompoundName)
        {
            Functions.ValidateRowNotExist(Compounds.CompoundTable, CompoundName);
        }

        [When(@"I create a new StagingArea called ""(.*)""")]
        public void WhenICreateANewStagingAreaCalled(string NewStagingArea)
        {
            Actiontype.clickAndWait(ElluminateHome.Administration, "2000");
            Actiontype.clickAndWait(ElluminateHome.Administation_Configuration, "2000");
            Actiontype.clickAndWait(StagingAreas.StagingAreaslink, "2000");
            Actiontype.clickAndWait(StagingAreas.AddStagingArea, "2000");            
            Actiontype.EnterValue(StagingAreas.Name, NewStagingArea);
            Actiontype.EnterValue(StagingAreas.Discription, NewStagingArea);
            Actiontype.clickAndWait(StagingAreas.Save, "5000");            
        }

        [When(@"I delete a StagingArea called ""(.*)""")]
        public void WhenIDeleteAStagingAreaCalled(string StagingArea)
        {
            Functions.DeleteRow(StagingAreas.StagingAreaTable, StagingArea);
        }
        
        [When(@"I create a new DataMart called ""(.*)""")]
        public void WhenICreateANewDataMartCalled(string DataMart)
        {
            Actiontype.clickAndWait(ElluminateHome.Administration, "2000");
            Actiontype.clickAndWait(ElluminateHome.Administation_Configuration, "2000");
            Actiontype.clickAndWait(DataMarts.DataMartslink, "2000");
            Actiontype.clickAndWait(DataMarts.AddDataMarts, "2000");
            Actiontype.EnterValue(DataMarts.Name, DataMart);
            Actiontype.EnterValue(DataMarts.Discription, DataMart);
            Actiontype.clickAndWait(DataMarts.Save, "5000");
        }
        
        [When(@"I delete a DataMart called ""(.*)""")]
        public void WhenIDeleteADataMartCalled(string DataMart)
        {
            Functions.DeleteRow(DataMarts.DataMartsTable, DataMart);
        }
        
        [Then(@"I should see StagingArea ""(.*)""")]
        public void ThenIShouldSeeStagingArea(string StagingArea)
        {
            Functions.ValidateRowExist(StagingAreas.StagingAreaTable,StagingArea);
        }
        
        [Then(@"I edit a StagingArea called ""(.*)"" to ""(.*)""")]
        public void ThenIEditAStagingAreaCalledTo(string StagingAreaNew, string StagingAreaEdit)
        {
            Functions.EditROw(StagingAreas.StagingAreaTable , StagingAreaNew, StagingAreaEdit);
        }
        
        [Then(@"I should not see StagingArea ""(.*)""")]
        public void ThenIShouldNotSeeStagingArea(string StagingArea)
        {
            Functions.ValidateRowNotExist(StagingAreas.StagingAreaTable, StagingArea);
        }
        
        [Then(@"I should see DataMart ""(.*)""")]
        public void ThenIShouldSeeDataMart(string DataMart)
        {
            Functions.ValidateRowExist(DataMarts.DataMartsTable, DataMart);
        }
        
        [Then(@"I edit a DataMart called ""(.*)"" to ""(.*)""")]
        public void ThenIEditADataMartCalledTo(string DataMartNew, string DataMartEdit)
        {
            Functions.EditROw(DataMarts.DataMartsTable, DataMartNew, DataMartEdit);
        }
        
        [Then(@"I should not see DataMart ""(.*)""")]
        public void ThenIShouldNotSeeDataMart(string DataMart)
        {
            Functions.ValidateRowNotExist(DataMarts.DataMartsTable, DataMart);
        }
    }
}
