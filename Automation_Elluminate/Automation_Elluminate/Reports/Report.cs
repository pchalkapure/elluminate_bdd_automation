﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Automation_Elluminate;
using System.Threading;
//using Automation_Elluminate.TestCases;
//using Automation_Elluminate.Testprocedure;
using RelevantCodes.ExtentReports;
using System.Drawing;

namespace Automation_Elluminate
{
    class TestReport
    {
        public static void Takescreenshot(String Status, String Message)
        {
            
            if (!System.IO.Directory.Exists(Report.reportPath))
            {
                System.IO.Directory.CreateDirectory(Report.reportPath);
            }
            Variables.ScreenShotLocation = Report.reportPath + Variables.currentScenarioName + DateTime.Now.ToString(Variables.dateTimeFormat) + Variables.screenshotExtension;
                                  
            Rectangle bounds = System.Windows.Forms.Screen.GetBounds(Point.Empty);
            using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);
                }
                bitmap.Save(Variables.ScreenShotLocation, System.Drawing.Imaging.ImageFormat.Png);
            }

            if (Status.ToUpper().Equals("PASSED"))
            {
                Report.test.Log(LogStatus.Pass, Message + Report.test.AddScreenCapture(Variables.ScreenShotLocation));
            }
            else
            {

                Report.test.Log(LogStatus.Fail, Message + Report.test.AddScreenCapture(Variables.ScreenShotLocation));
            }

        }
    }
}
