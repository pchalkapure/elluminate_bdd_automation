﻿Feature: Elluminate_EForms

Background:
	Given We are at the Sign In Page
	And I login "AutoTesting", "Auto@403" and "SmokeTest, Auto"

Scenario:Elluminate_VerifyeFormsCreation_SmokeTest
	Given I select eForms Designer from the Administration DropDown
	Then Verify Study dropdown, PublishALL and AddForm fields exists
	And Create New Form for "AutoStudy" study and "Operational" Data Store with below details
	| FormName       | FormLabel       | FormCategory       |
	| AutoStudy_Form | AutoStudy_Label | AutoStudy_Category |
	Then Delete the form
@ignore
Scenario:Elluminate_VerifyFieldsCreation_SmokeTest
	Given I select eForms Designer from the Administration DropDown
	Then Verify Study dropdown, PublishALL and AddForm fields exists
	And Create New Form for "AutoStudy" study and "Operational" Data Store with below details
	| FormName       | FormLabel       | FormCategory       |
	| AutoStudy_Form | AutoStudy_Label | AutoStudy_Category |
	Then Add New "textbox" Field into the form with below details
	| FieldName | FieldLabel   | FieldDisplayOrd  |
	| Test1_Name | Test1_Label | 1                |
	Then Delete the form
@ignore
Scenario:Elluminate_VerifyRecordCreation&Modification_SmokeTest
    Given I select eForms Designer from the Administration DropDown
	Then Verify Study dropdown, PublishALL and AddForm fields exists
	And Create New Form for "AutoStudy" study and "Operational" Data Store with below details
	| FormName       | FormLabel       | FormCategory       |
	| AutoStudy_Form | AutoStudy_Label | AutoStudy_Category |
	Then Add New "textbox" Field into the form with below details
	| FieldName | FieldLabel   | FieldDisplayOrd  |
	| Test1_Name | Test1_Label | 1                |
	And Navigate to eForms tab
	Then Select the "AutoStudy" study from the drop-down
	And Click on "AutoStudy_Category" Category and "AutoStudy_Form" Form
	Then Create New Record with "RecordData1" data
	And Edit the Record data as "RecordData2" 
	Then Delete the Record data
	Given I select eForms Designer from the Administration DropDown
	Then Select the "AutoStudy" study from the drop-down
	And Click on "AutoStudy_Category" Category and "AutoStudy_Form" Form
	Then Delete the form