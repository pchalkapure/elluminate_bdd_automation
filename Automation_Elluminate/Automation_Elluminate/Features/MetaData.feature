﻿Feature: Elluminate_MetaData
	

Background:
    Given We are at the Sign In Page
	And I login "AutoTesting", "Auto@403" and "SmokeTest, Auto"
	When Go to MetaData home page


Scenario:Elluminate_MetaData_Browse_SmokeTest	
	Given Select Browse tab
	Then I should see Study called "AutoStudy" in metadata studies list 
	When I select Study "AutoStudy" from metadata studies list 
	Then I should see Imported DataStore in the right side page with columns
	|Data Store|Domains|Variables|Created On|

	When I Have expand DataStores
	Then I should see all Domain lists with columns
	|Domain Name|Domain Label|Variable Count|

	When I Have expand Domain name
	Then I should see all Variables lists
	|Variable Name|Variable Label|Data Type|Length|
	
	
		
Scenario:Elluminate_MetaData_Search_SmokeTest
	Given Select Search tab
	Then I should see Study called "AutoStudy" in metadata search studies list 
	When I select Study "AutoStudy" from metadata search studies list
	And I select For "Domain" WithIn "Names" and Condition "Start With" value "AE" 
	Then the result should be display with columns in Results section
	|Study|Data Store|Domain Name|Domain Label|Variable Count|

	And Veify all results Startwith DomainName "AE"
	When I select For "Variables" WithIn "Labels" and Condition "Ends With" value "E" 
	Then the result should be display with columns in Results section
	|Study|Data Store|Domain Name|Variable Name|Variable Label|Data Type|Length|SAS Format|

	And Veify all results Endswith VariableName "AE"

	

Scenario:Elluminate_MetaData_Compare_SmokeTest
	Given Select Compare tab
	And I Select DataStore for compare
	Then I should see Study called "AutoStudy" in studies list 
	When I select Study "AutoStudy" from studies list
	And I select Domain "AE" from Domain list
	Then Data should load for selected study and domin 


