﻿Feature: Elluminate_Mapper
		
Background:
    Given We are at the Sign In Page
	And I login "AutoTesting", "Auto@403" and "SmokeTest, Auto"
	

Scenario:Elluminate_Mappingdomains_LeftJoin_SmokeTest 
	Given I choose "MapperTest" Study from studies list.
    Given select mapper Tab 
	Then  I should see Mapper home page
	And I Should Delete Existing mapping Defination and Domain
    When Create mapping Defination name called "Mapper_Defination" with DataStore "Clinical"
	Then I should see "Mapping_Defination" in mappings section
    When Creating mapping domain name called "Mapper_Domain"
   	Then I should see "Mapper_Domain" in Domains section
	Given Map "AE" and "DE" using Left Join Condition 
	When I execute mapping
	Then verify mapping status
	And I should see mapped domain "Mapper_Domain" in DataStore "Clinical"
	And I Should Delete Existing mapping Defination and Domain
