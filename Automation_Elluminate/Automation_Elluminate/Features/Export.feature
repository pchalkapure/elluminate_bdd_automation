﻿Feature: Elluminate_Export

		
Background:
    Given We are at the Sign In Page
	And I login "AutoTesting", "Auto@403" and "SmokeTest, Auto"

Scenario:Elluminate_Export_StagingArea_SmokeTest

Given I should see Study called "AutoStudy"
And I should Select Study called "AutoStudy"
When I select Export Tab from the menu
And I Select DataStore is "Clinical"
And I Select Available Domains
And CLick on Export button 
Then Verify Exprot process For DataStore "Clinical" , Formate as "XLSX" and Status "Completed" 
And Download the exported file 


Scenario:Elluminate_Export_Delimited_Tilde_SmokeTest

Given I should see Study called "AutoStudy"
And I should Select Study called "AutoStudy"
When I select Export Tab from the menu
And I Select DataStore is "Clinical"
And I Select Available Domains
And I Select Data Formate is Delimited with Tilde Delimiter
And CLick on Export button 
Then Verify Exprot process For DataStore "Clinical" , Formate as "DLM ( ~ )" and Status "Completed" 
And Download the exported file 

Scenario:Elluminate_Export_Delimited_VerticalBar_SmokeTest

Given I should see Study called "AutoStudy"
And I should Select Study called "AutoStudy"
When I select Export Tab from the menu
And I Select DataStore is "Clinical"
And I Select Available Domains
And I Select Data Formate is Delimited with Vertical Bar Delimiter
And CLick on Export button 
Then Verify Exprot process For DataStore "Clinical" , Formate as "DLM ( | )" and Status "Completed" 
And Download the exported file 

Scenario:Elluminate_Export_DataMart_SmokeTest

Given I choose "AutoStudy" Study from studies list.  
When I go to Importer tab
Then I Clear the data within Importer tab
Given Go to Data Sources tab
Given Clear the data within Data Sources tab
When I create a new "FTP" Data Source with below details
| Name        | AddressUrl                   | UserName | Password   | Remotefoldername | Remotefiles  | Description         |
| Auto_DS_FTP | ftpes://ftp.eclinicalsol.com | cdara    | password.1 | Chandu           | TestData.zip | FTP Test Descrption |
When I go to Importer tab
Then I create import definition for "FTP" with Data Source is "Auto_DS_FTP" and DataStore is "Reporting"
And Run and clear import definitions
When I select Export Tab from the menu
And I Select DataStore is "Reporting"
And I Select Available Domains
And CLick on Export button 
Then Verify Exprot process For DataStore "Reporting" , Formate as "XLSX" and Status "Completed" 
And Download the exported file 


Scenario:Elluminate_Export_StagingArea_FTPOutBound_SmokeTest

Given I choose "AutoStudy" Study from studies list.  
When I go to Importer tab
Then I Clear the data within Importer tab
Given Go to Data Sources tab
Given Clear the data within Data Sources tab
When I create a new "FTP Outbound" Data Source with below details
| Name                | AddressUrl                   | UserName | Password   | Remotefoldername             | Remotefiles  | Description                   |
| Auto_DS_FTPOutBound | ftpes://ftp.eclinicalsol.com | cdara    | password.1 | /ftpdata/cdr/client1/Chandu/ | TestData.zip | FTP out bound Test Descrption |
When I select Export Tab from the menu
And I Select DataStore is "Clinical"
And I Select Available Domains
And I Select Export FTP Outbound checkbox
And CLick on Export button 
Then Verify Exprot process For DataStore "Clinical" , Formate as "XLSX" and Status "Completed" 
And Download the exported file 

Scenario:Elluminate_Export_Schedule_SmokeTest

Given I choose "AutoStudy" Study from studies list.  
When I select Export Tab from the menu
And I Select DataStore is "Clinical"
And I Select Available Domains
And I Schedule Task for Export process 
 Given Select Task tab
 Then Verify Schedule Export Added to task list
 And Verify Schedule process is executed successfully 