﻿Feature: Elluminate_Sanpshot
	
Background:  
   Given We are at the Sign In Page
   And I login "AutoTesting", "Auto@403" and "SmokeTest, Auto"
   Given I choose "MapperTest" Study from studies list.
   

Scenario: Elluminate_Snapshot_Add_Edit_Delete_SmokeTest
	Given Go to Snapshot tab
	Then create new Snapshot "Snapshot_Nov20"
	Then Edit the Snapshot
	Then verify snapshot title "Snapshot_Nov_2017"
	Then Delete the Snapshot


