﻿Feature: Elluminate_Study
	In order to import data to a study
	I need to create a study
	which needs a compound
	

Background:
    Given We are at the Sign In Page
	And I login "AutoTesting", "Auto@403" and "SmokeTest, Auto"
    
	
Scenario:Elluminate_Study_SmokeTest	
	When I create a new compound called "AutoCompound_Test"
	Then I should see compound"AutoCompound_Test"   
	Given We are at the study dashboard page
	When I add a new study named "AutoStudy_Test"
	And I choose "AutoCompound_Test" as the compound.
	And I save the study
	Then I should see "AutoStudy_Test" on the dashboard.
	When I delete "AutoStudy_Test"
	Then I should not see study "AutoStudy_Test"
	Given We are at the study dashboard page
	And I delete a compound called "AutoCompound_Test"